pragma solidity 0.4.23;

import '../SigToken.sol';


/// @title Helper for unit-testing SigToken - DONT use in production!
contract SigTokenTestHelper is SigToken {

    function SigTokenTestHelper(address[] _initialOwners, uint _signaturesRequired)
        public
        SigToken(_initialOwners, _signaturesRequired)
    {
    }
}
